import React from 'react';
import SetPattern from './components/SetPattern.jsx'

class App extends React.Component{

	constructor(props){
		super(props);
		this.state={gridSize: 3};
	}



	render(){
		return (
		<div>
			<SetPattern gridSize={this.state.gridSize}/>
		</div>
		);
	}
}

export default App;