import React from 'react';

class Pattern extends React.Component{

    render() {
    	return( 
    		<svg>
    		{
                this.props.cordinates.length>0?
                <line 
                    x1= {100*this.props.cordinates[this.props.cordinates.length-1].x} 
                    y1= {100*this.props.cordinates[this.props.cordinates.length-1].y}
                    x2= {this.props.mouseX}
                    y2= {this.props.mouseY} 
                    strokeWidth="3" stroke={this.props.stroke}
                />:null        	
            }
        	</svg>
		);
    }
}

export default Pattern;