import React from 'react';

class GridPoints extends React.Component{

    constructor(props) {
        super(props);
        this.state={
            array: []
        }
    }

    componentWillMount(){
        var array = this.formArray(this.props.maxSize)
        this.setState({array})
    }

    componentWillReceiveProps(nextProps){
        if(this.props.maxSize!=nextProps.maxSize){
            var array = this.formArray(this.props.gridSize)
            this.setState({array})
        }
    }

    formArray = (size) => {
        var array = []
        for(var i=3; i<=size; i++)
            array.push(i)
        return array;
    }


    render() {
    	return( 
            <div>
                <label>Select Size of Grid: </label>
        		<select onChange={this.props.onGridSizeChange} value={this.props.gridSize}>
                    {
                        this.state.array.map((data)=> 
                            <option key={data} value={data}>{data + "X" + data}</option>
                    )}
                </select>
                <br/> <br/> <br/>
            </div>
		);
    }
}

export default GridPoints;