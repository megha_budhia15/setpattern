import React from 'react';

class Pattern extends React.Component{

    render() {
    	return( 
    		<svg>
    		{
        		this.props.cordinates.map((value, k)=>
        			(k+1)<this.props.cordinates.length?
        			<line 
        				key={"line"+k}
        				x1= {100*value.x} 
        				y1= {100*value.y}
        				x2= {100*this.props.cordinates[k+1].x}
        				y2= {100*this.props.cordinates[k+1].y} 
        				strokeWidth="3" stroke={this.props.stroke}
        			/>:null
        		)
        	}
        	</svg>
		);
    }
}

export default Pattern;