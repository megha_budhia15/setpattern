import React from 'react';
import GridPoints from './GridPoints.jsx'
import Pattern from './Pattern.jsx'
import CreatingLine from './CreatingLine.jsx'
import Dropdown from './Dropdown.jsx'

class SetPattern extends React.Component{

    constructor(props) {
        super(props);
        this.state = { 
            cordinates: [],
            validateCordinates: [],
            mouseX:200,
            mouseY:200,
            mode: "NO_PATTERN",
            gridSize: 3,
            gridLength: 400
        };
        this.__pt={}
        this.__svg = {};
        //Msgs for each mode
        this.__modes={
            NO_PATTERN:          "Create pattern by joining points",
            CREATING_PATTERN:    "Recording Pattern... Drag mouse out of black box when done!",
            INVALID_PATTERN:     "Invalid pattern! Select atleast 2 points",
            SHOW_PATTERN:        "Succesfully recorded this pattern!",
            HIDE_PATTERN:        "Current Pattern Hidden",
            VALIDATING_PATTERN:  "Draw pattern for validation and then reset",
            PATTERN_VALIDATED:   "Succesfully Validated! Resetting current pattern...",
            INCORRECT_PATTERN:   "Invalid Pattern! Cannot reset pattern..."
        }
        //Pattern display color
        this.__patternColor={
            CREATING_PATTERN: "white",
            SHOW_PATTERN: "blue",
            VALIDATING_PATTERN: "white",
            PATTERN_VALIDATED: "blue",
            INCORRECT_PATTERN: "red"
        }
    }

    componentDidMount(){
        this.__svg = document.querySelector('svg');
        this.__pt = this.__svg.createSVGPoint();
        this.__lastCordinate=""
    }

    //Records mouse position inside the grid
    handleMouseMove = (evt)  =>{
        if(this.state.mode=="CREATING_PATTERN" || this.state.mode=="VALIDATING_PATTERN"){
            let pt= this.__pt
            pt.x = evt.clientX;
            pt.y = evt.clientY;
            var cursorpt =  pt.matrixTransform(this.__svg.getScreenCTM().inverse());
            let x= Math.round(cursorpt.x)
            let y= Math.round(cursorpt.y)
            this.setState({ mouseX: x, mouseY:y});
        }
    }

    // Handles mouse hover on points on grid
    handleMouseOver=(i,j) => {
        if(this.state.mode=="CREATING_PATTERN"  ){
            let cordinates= this.state.cordinates
            let data= this.pushCordinateIntoPattern(cordinates, i,j)
            if(data)
                this.setState({cordinates: data})
        }
        else if(this.state.mode=="VALIDATING_PATTERN"){
            let cordinates= this.state.validateCordinates
            let data= this.pushCordinateIntoPattern(cordinates, i, j)
            if(data)
                this.setState({validateCordinates: data})
        }
    }

    //verifies that the touched point not used the pattern earlier and then records it onto the pattern
    pushCordinateIntoPattern = (cordinates, i,j) => {
        let flag=true
        for(var k=0; k<cordinates.length; k++){
            if(cordinates[k].x==i && cordinates[k].y==j ){
              flag=false
              break ;
            }
        }
        if(flag){
            cordinates.push({x:i, y:j})
            return cordinates;
        }
        else
            return false;
    }

    handleMouseEnter =() => {
        if(this.state.mode=="NO_PATTERN" || this.state.mode=="INVALID_PATTERN"){
            this.setState({mode: 'CREATING_PATTERN'})
        }
    } 

    handleMouseLeave =() => {
        //verifies and sets new pattern 
        if(this.state.mode=="CREATING_PATTERN"){
            if(this.state.cordinates.length<2)
                this.setState({mode: 'INVALID_PATTERN', cordinates: []})
            else
                this.showCurrentPattern()
        }// Valdates enter pattern matches actuall pattern
        else if(this.state.mode=="VALIDATING_PATTERN"){
            let cordinates= this.state.cordinates
            let validateCordinates= this.state.validateCordinates
            let flag=true
            for(var k=0; k<validateCordinates.length; k++){
                if(!cordinates[k] || cordinates[k].x!=validateCordinates[k].x || cordinates[k].y!=validateCordinates[k].y){
                    flag=false
                    break;
                }
            }
            if(flag)
              this.succesfullResetPattern();
            else
              this.unSuccesfullResetPattern();
        } 
    } 

    showCurrentPattern = () => {
        this.setState({mode: 'SHOW_PATTERN'})
        setTimeout(()=> {
            this.setState({mode: 'HIDE_PATTERN'})
        },2000)
    }

    resetPattern = () => {
        this.setState({mode: 'VALIDATING_PATTERN', validateCordinates: []})
    }

    succesfullResetPattern = () => {
        this.setState({mode: 'PATTERN_VALIDATED', validateCordinates: []})
        setTimeout(()=> {
            this.setState({mode: 'NO_PATTERN', cordinates: []})
        },2000)
    }

    unSuccesfullResetPattern = () => {
        this.setState({mode: 'INCORRECT_PATTERN'})
        setTimeout(()=> {
            this.setState({mode: 'HIDE_PATTERN', validateCordinates: []})
        },1000)
    }

    onGridSizeChange = (e) => {
        let gridSize = Number(e.target.value)
        let gridLength = 100*(gridSize+1)
        this.setState({gridSize, gridLength})
    }


    render() {
    	return( 
    		<div>
	            <h3>{this.__modes[this.state.mode]}</h3>
                {   this.state.mode=='NO_PATTERN'?
                    <Dropdown maxSize={7} gridSize={this.state.gridSize} onGridSizeChange={this.onGridSizeChange}/>: null
                }
                <svg 
                    width={this.state.gridLength}
                    height={this.state.gridLength}
                    onMouseMove={this.handleMouseMove} 
                    viewBox={"0 0 "+ this.state.gridLength + " " + this.state.gridLength}
                    onMouseEnter={this.handleMouseEnter} 
                    onMouseLeave={this.handleMouseLeave}
                >
                    <rect x="0" y="0" height={this.state.gridLength} width={this.state.gridLength} fill="black"/>
                    {this.state.mode=='HIDE_PATTERN'? null:
                        <GridPoints gridSize={this.state.gridSize} handleMouseOver={this.handleMouseOver} />
                    }
                    {this.state.mode=='HIDE_PATTERN'? null:
                        <Pattern
                            cordinates= {this.state.mode=='VALIDATING_PATTERN' || this.state.mode=='INCORRECT_PATTERN' ? this.state.validateCordinates :this.state.cordinates }
                            stroke={this.__patternColor[this.state.mode]}
                        />
                    }
                    { 
                        this.state.mode=='CREATING_PATTERN' || this.state.mode=='VALIDATING_PATTERN' ?
                        <CreatingLine
                            cordinates= {this.state.mode=='CREATING_PATTERN'? this.state.cordinates: this.state.validateCordinates}
                            mouseX= {this.state.mouseX}
                            mouseY= {this.state.mouseY} 
                            stroke="white"
                        />:null
                    }
                </svg>
                <br/>
                <br/>
                { this.state.mode=='HIDE_PATTERN' || this.state.mode=='SHOW_PATTERN'?
                    <button onClick={this.resetPattern}>Validate to Reset Pattern</button>:null
                }
                {this.state.mode=='HIDE_PATTERN'? 
                    <button onClick={this.showCurrentPattern}>Show Current Pattern</button> : null
                }
        	</div>
		);
    }
}

export default SetPattern;