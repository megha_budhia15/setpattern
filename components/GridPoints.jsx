import React from 'react';

class GridPoints extends React.Component{

    constructor(props) {
        super(props);
        this.state={
            array: []
        }
    }

    componentWillMount(){
        var array = this.formArray(this.props.gridSize)
        this.setState({array})
    }

    componentWillReceiveProps(nextProps){
        if(this.props.gridSize!=nextProps.gridSize){
            var array = this.formArray(nextProps.gridSize)
            this.setState({array})
        }
    }

    formArray = (size) => {
        var array = []
        for(var i=1; i<=size; i++)
            array.push(i)
        return array;
    }

    render() {
    	return( 
    		<svg>
    		{
                this.state.array.map((i)=> 
                    this.state.array.map((j) => 
                        <circle key={(i-1)*3 + j} cx={100*i} cy={100*j} r="7" fill="green" onMouseOver={()=> this.props.handleMouseOver(i,j)}/>

                    )
                )
            }
        	</svg>
		);
    }
}

export default GridPoints;